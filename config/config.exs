import Config

config :wampex_client, env: config_env()

import_config "#{Mix.env()}.exs"
