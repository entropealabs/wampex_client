defmodule Wampex.Client.MixProject do
  use Mix.Project

  def project do
    [
      app: :wampex_client,
      version: "0.2.0",
      elixir: "~> 1.17",
      elixirc_options: [warnings_as_errors: true],
      start_permanent: Mix.env() == :prod,
      consolidate_protocols: Mix.env() == :prod,
      elixirc_paths: ~w[lib],
      aliases: aliases(),
      deps: deps(),
      description: description(),
      package: package(),
      docs: [
        main: "readme",
        extras: ["README.md"],
        assets: "assets"
      ]
    ]
  end

  def description do
    "WAMP Client"
  end

  def package do
    [
      name: :wampex_client,
      files: ["lib", "priv", "mix.exs", "README*", "LICENSE*"],
      maintainers: ["Christopher Steven Coté"],
      licenses: ["MIT License"],
      links: %{
        "GitLab" => "https://gitlab.com/entropealabs/wampex_client"
      }
    ]
  end

  def application do
    [
      extra_applications: [:logger],
      env: [env: Mix.env()]
    ]
  end

  defp deps do
    [
      {:credo, "~> 1.7", only: ~w[dev test]a, runtime: false},
      {:dialyxir, "~> 1.4", only: ~w[dev test]a, runtime: false},
      {:ex_doc, ">= 0.36.1", only: :dev, runtime: false},
      {:states_language, "~> 0.4"},
      {:wampex, "~> 0.2"},
      {:websockex, "~> 0.4"}
    ]
  end

  defp aliases do
    [
      all_tests: [
        "compile --force --warnings-as-errors",
        "credo --strict",
        "format --check-formatted",
        "dialyzer --halt-exit-status"
      ]
    ]
  end
end
