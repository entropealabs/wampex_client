defmodule Wampex.Client.Transport do
  @moduledoc "Behaviour for Transports"
  @callback send_request(transport :: atom() | pid(), message :: Wampex.message()) :: :ok
  @callback start_link(
              url: binary(),
              session: Wampex.Client.Session.t(),
              protocol: binary(),
              serializer: module(),
              opts: []
            ) ::
              {:ok, pid}
              | {:error, term}
end
